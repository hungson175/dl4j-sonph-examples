package org.deeplearning4j.examples.sonph.examples;

import org.datavec.api.conf.Configuration;
import org.datavec.api.records.reader.RecordReader;

import org.datavec.api.records.reader.impl.misc.LibSvmRecordReader;
import org.datavec.api.records.reader.impl.misc.SVMLightRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.io.File;
import java.io.IOException;

public class LearnReadLibsvm {
    private static final String TRAIN_LIBSVM = "/home/hungson175/Data/Other/ownsample.libsvm";

    public static void main(String[] args) throws IOException, InterruptedException {
        new LearnReadLibsvm().start();
    }

    private void start() throws IOException, InterruptedException {

        int numOfFeatures = 5;
        int batchSize = 2;
        int numClasses = 2;

        Configuration config = new Configuration();
        config.setBoolean(SVMLightRecordReader.ZERO_BASED_INDEXING, true);
        config.setInt(SVMLightRecordReader.NUM_FEATURES, numOfFeatures );

        SVMLightRecordReader trainReader = new SVMLightRecordReader();
        trainReader.initialize(config, new FileSplit(new File(TRAIN_LIBSVM)));
        DataSetIterator iterator = new RecordReaderDataSetIterator(trainReader, batchSize, numOfFeatures, numClasses);

        while ( iterator.hasNext() ) {
            DataSet data = iterator.next();
            INDArray featureMatrix = data.getFeatureMatrix();
            System.out.println("==========\nFeature: " + featureMatrix);
            INDArray labels = data.getLabels();
            System.out.println("==========\nLabels: " + labels);
        }
    }

}
