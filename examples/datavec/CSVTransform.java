package org.deeplearning4j.examples.sonph.examples.datavec;

import com.sun.xml.internal.rngom.ast.builder.SchemaBuilder;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.transform.TransformProcess;
import org.datavec.api.transform.schema.Schema;
import org.datavec.api.writable.Writable;
import org.datavec.spark.transform.SparkTransformExecutor;
import org.datavec.spark.transform.misc.StringToWritablesFunction;
import org.datavec.spark.transform.misc.WritablesToStringFunction;

import java.util.List;

public class CSVTransform {
    public static void main(String[] args) {
        int numLinesToSkip = 0;
        String delimiter = ",";

        String baseDir = "/home/hungson175/Downloads/";
        String prefixFileName = "reports";
        String extensionFileName = "csv";
        String fileName = prefixFileName + "." + extensionFileName;
        String inputPath = baseDir + fileName;
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String outputPath = baseDir + prefixFileName + "_processed_" + timeStamp + "." + extensionFileName;


        /**
         * input schema
         */
        Schema inputDataSchema = new Schema.Builder()
            .addColumnsString("datetime", "serverity", "location", "county", "state")
            .addColumnsDouble("lat", "lon")
            .addColumnsString("comment")
            .addColumnCategorical("type", "TOR", "WIND", "HAIL")
            .build();

        /**
         * Transformation process
         */
        TransformProcess tp = new TransformProcess.Builder(inputDataSchema)
            .removeColumns("datetime", "serverity", "location", "county", "state", "comment")
            .categoricalToInteger("type")
            .build();

        int numActions = tp.getActionList().size();
        for (int i = 0; i < numActions; i++) {
            System.out.println("\n\n=========================");
            System.out.println("---Schema after step " + i + " (" + tp.getActionList().get(i) + " ) ---");
            System.out.println(tp.getSchemaAfterStep(i));
        }


        SparkConf sparkConf = new SparkConf();
        sparkConf.setMaster("local[*]");
        sparkConf.setAppName("Storm Report Record Reader Transform");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);


        // read the data file
        JavaRDD<String> lines = sc.textFile(inputPath);
        // convert to Writable
        JavaRDD<List<Writable>> stormReports = lines.map(new StringToWritablesFunction(new CSVRecordReader()));
        // run our transform process
        JavaRDD<List<Writable>> processed = SparkTransformExecutor.execute(stormReports,tp);
        // convert Writable back to string for export
        JavaRDD<String> toSave= processed.map(new WritablesToStringFunction(","));

        toSave.saveAsTextFile(outputPath);

    }
}
