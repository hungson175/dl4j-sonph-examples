package org.deeplearning4j.examples.sonph.examples;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.transform.MathFunction;
import org.datavec.api.transform.MathOp;
import org.datavec.api.transform.TransformProcess;
import org.datavec.api.transform.schema.Schema;
import org.datavec.api.transform.transform.time.DeriveColumnsFromTimeTransform;
import org.datavec.api.writable.Writable;
import org.datavec.spark.transform.SparkTransformExecutor;
import org.datavec.spark.transform.misc.StringToWritablesFunction;
import org.datavec.spark.transform.misc.WritablesToStringFunction;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class LearnParseDateTime {
    private static final String DATA_FILE = "/home/hungson175/Data/Other/LearnDateTimeParsing.csv";
    private static final String PROCESSED_FILE = "/home/hungson175/Data/Other/LearnDateTimeParsing_processed.csv";
    private static JavaSparkContext sc;

    public static void main(String[] args) throws IOException {
        Schema inputSchema = new Schema.Builder()
            .addColumnsString("name", "date").build();

        TransformProcess tp = new TransformProcess.Builder(inputSchema)
            .stringToTimeTransform("date", "yyMMddHH", DateTimeZone.UTC)
            .transform(
                new DeriveColumnsFromTimeTransform.Builder("date")
                    .addIntegerDerivedColumn("hourOfDay", DateTimeFieldType.hourOfDay())
                .build())
            .removeColumns("date")
            .duplicateColumn("hourOfDay", "hourOfDayDouble")
            .convertToDouble("hourOfDayDouble")
            .doubleMathOp("hourOfDayDouble",MathOp.Divide, 24)
            .doubleMathOp("hourOfDayDouble",MathOp.Multiply, 2 * Math.PI)
            .duplicateColumn("hourOfDayDouble","sinHour")
            .doubleMathFunction("sinHour",MathFunction.SIN)
            .duplicateColumn("hourOfDayDouble","cosHour")
            .doubleMathFunction("cosHour",MathFunction.COS)
            .integerToOneHot("hourOfDay",0,23)
            .build();

        SparkConf sparkConf = new SparkConf();
        sparkConf.setMaster("local[*]");
        sparkConf.setAppName("Avazu Record Reader Transform");
        sc = new JavaSparkContext(sparkConf);

        JavaRDD<String> lines = sc.textFile(DATA_FILE);
        // convert to Writable
        JavaRDD<List<Writable>> samples = lines.map(new StringToWritablesFunction(new CSVRecordReader()));
        // run our transform process
        JavaRDD<List<Writable>> processed = SparkTransformExecutor.execute(samples,tp);
        // convert Writable back to string for export
        JavaRDD<String> toSave= processed.map(new WritablesToStringFunction(","));

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(PROCESSED_FILE))) {
            List<String> list = toSave.collect();
            list.forEach(line -> printWriter.println(line));
        }
//        toSave.saveAsTextFile(PROCESSED_FILE);


    }

}
