package org.deeplearning4j.examples.sonph.examples;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerMinMaxScaler;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class LearnNormalize {
    private static Logger log = LoggerFactory.getLogger(LearnNormalize.class);
    public static void main(String[] args) throws IOException, InterruptedException {
        RecordReader recordReader = new CSVRecordReader();
        recordReader.initialize(new FileSplit(new File("/home/hungson175/Data/Other/NormalizeSample.csv")));
        RecordReaderDataSetIterator iterator = new RecordReaderDataSetIterator(recordReader, 1);
        DataNormalization normalizer = new NormalizerStandardize();
        normalizer.fit(iterator);
        iterator.setPreProcessor(normalizer);
        while ( iterator.hasNext()) {
            DataSet batch = iterator.next();
            normalizer.transform(batch);
            System.out.println(batch.getFeatureMatrix());

//            log.info("\nAfter: {}",batch);
        }
    }
}
