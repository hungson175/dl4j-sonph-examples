package org.deeplearning4j.examples.sonph.examples;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.util.ModelSerializer;
import org.jcodec.common.Assert;
import org.jetbrains.annotations.NotNull;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.IUpdater;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class Helpers {


    public static void splitData(
        String originalFile,
        String trainFile,
        String testFile,
        long seed,
        double trainSplitRatio) throws IOException {
        Random random = new Random(seed);
        try (BufferedReader br = new BufferedReader(new FileReader(originalFile)) ;
             PrintWriter trainWriter = new PrintWriter(new FileWriter(trainFile)) ;
             PrintWriter testWriter = new PrintWriter(new FileWriter(testFile)) ;
        ) {
            String line = null;
            while ( ( line = br.readLine()) != null) {
                double v = random.nextDouble();
                if ( v < trainSplitRatio ) {
                    trainWriter.println(line);
                } else {
                    testWriter.println(line);
                }

            }
        }

    }

    public static MultiLayerNetwork readModel(String fullModelPath) throws IOException {
        return ModelSerializer.restoreMultiLayerNetwork(new File(fullModelPath));
    }

    public static MultiLayerConfiguration createNN(
        int nIn,
        int nOut,
        int[] nHiddenLayers,
        IUpdater updater,
        double regL2,
        LossFunctions.LossFunction lossFn,
        Activation outputActFn,
        long seed) {

        NeuralNetConfiguration.ListBuilder list = new NeuralNetConfiguration.Builder()
            .seed(seed) //include a random seed for reproducibility
            .weightInit(WeightInit.XAVIER)
            .updater(updater)
            .l2(regL2) // regularize learning model
            .list();
        int[] nAtLayers = new int[nHiddenLayers.length + 1];

        nAtLayers[0] = nIn;
        for (int i = 1; i < nAtLayers.length; i++) {
            nAtLayers[i] = nHiddenLayers[i - 1];
        }
        for (int i = 0; i < nAtLayers.length - 1; i++) {
            list.layer( i, new DenseLayer.Builder() //create the first input layer.
                .nIn(nAtLayers[i]).nOut(nAtLayers[i+1])
                .activation(Activation.RELU)
                .build());
        }
        list.layer(nAtLayers.length - 1, new OutputLayer.Builder(lossFn) //create hidden layer
            .activation(outputActFn)
            .nIn(nAtLayers[nAtLayers.length-1])
            .nOut(nOut)
            .build());

        return list.build();

    }

    @NotNull
    static File saveModel(Model model, String modelFileName) throws IOException {
        File locationToSave = new File(modelFileName);
        boolean saveUpdater = true;
        ModelSerializer.writeModel(model, locationToSave, saveUpdater);
        return locationToSave;
    }

    public static ArrayList<String> readLines(String filePath) {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line = null;
            ArrayList<String> list = new ArrayList<>();
            while (  ( line = br.readLine()) != null) {
                list.add(line.trim());
            }
            return list;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static double logLoss(List<Double> predicts, List<Integer> labels) {
        Assert.assertEquals(predicts.size(), labels.size());
        return IntStream.range(0, predicts.size())
            .mapToDouble(index -> rowLoss(labels, predicts, index))
            .average()
            .orElse(Double.MAX_VALUE);
    }

    private static final double EPS = 1e-9;
    private static double rowLoss(List<Integer> labels, List<Double> predicts, int index) {
        Integer yi = labels.get(index);
        Double pi = predicts.get(index);
        pi = Math.max(EPS, Math.min(1 - EPS, pi));
        return - (yi * Math.log(pi) + (1.0 - yi) * Math.log(1.0 - pi));
    }

    public static long longHashId(String userId) {
        long hashedId = 0L;
        int l = userId.length();
        char[] chars = userId.toCharArray();

        for (int i = 0; i < l; i++) {
            hashedId = 31 * hashedId + chars[i];
        }
        return hashedId;
    }
}
