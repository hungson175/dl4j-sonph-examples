package org.deeplearning4j.examples.sonph.examples;


import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.IOException;

import static org.deeplearning4j.examples.feedforward.regression.RegressionMathFunctions.learningRate;
import static org.deeplearning4j.examples.feedforward.regression.RegressionMathFunctions.numOutputs;

public class LearnBuilder {
    private static final int LABEL_INDEX = 0; //TODO: later
    private static final long SEED = 1;
    private static final double LEARNING_RATE = 0.01;
    public static final double L2_RATE = LEARNING_RATE * 0.1;
    private static final int BATCH_SIZE = 512;
    public static final int N_HIDDEN_NODES = 4000;
    private static final int N_EPOCHS = 10;
    public static final double MOMENTUM = 0.9;
    public static final String TRAIN_CSV = "/home/hungson175/Data/Avazu/train_subl_0p125_processed.csv";
    public static final String TEST_CSV = "/home/hungson175/Data/Avazu/test_subl_0p025_processed.csv";
    private static final int N_INPUTS = 212;
    private static final int N_OUTPUTS = 2;


    public static void main(String[] args) {

        try {

            RecordReader trainReader = new CSVRecordReader();
            trainReader.initialize(new FileSplit(new File(TRAIN_CSV)));
            DataSetIterator trainIter = new RecordReaderDataSetIterator(trainReader, BATCH_SIZE, LABEL_INDEX, 2);

            RecordReader testReader = new CSVRecordReader();
            testReader.initialize(new FileSplit(new File(TEST_CSV)));
            DataSetIterator testIter = new RecordReaderDataSetIterator(testReader, BATCH_SIZE, LABEL_INDEX, 2);



            MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(SEED)
                .updater(new Adam(LEARNING_RATE))
//                .updater(new Nesterovs(LEARNING_RATE, MOMENTUM))
                .l2(L2_RATE)
                .list()
                .layer(0, new DenseLayer.Builder().nIn(N_INPUTS).nOut(N_HIDDEN_NODES)
                    .weightInit(WeightInit.XAVIER)
                    .activation(Activation.RELU)
                    .build())
                .layer(1, new DenseLayer.Builder().nIn(N_HIDDEN_NODES).nOut(N_HIDDEN_NODES)
                    .weightInit(WeightInit.XAVIER)
                    .activation(Activation.RELU)
                    .build())
                .layer(2, new DenseLayer.Builder().nIn(N_HIDDEN_NODES).nOut(N_HIDDEN_NODES)
                    .weightInit(WeightInit.XAVIER)
                    .activation(Activation.RELU)
                    .build())
                .layer(3, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                    .weightInit(WeightInit.XAVIER)
                    .activation(Activation.SOFTMAX)
                    .nIn(N_HIDDEN_NODES).nOut(N_OUTPUTS).build())
                .pretrain(false).backprop(true).build();

            MultiLayerNetwork model = new MultiLayerNetwork(conf);
            model.init();
            model.setListeners(new ScoreIterationListener(5));    //Print score every 100 parameter updates

            for ( int n = 0; n < N_EPOCHS; n++) {
                model.fit( trainIter );
            }

            System.out.println("Evaluate model....");
            Evaluation eval = new Evaluation(N_OUTPUTS);
            while(testIter.hasNext()){
                DataSet t = testIter.next();
                INDArray features = t.getFeatureMatrix();
                INDArray labels = t.getLabels();
                INDArray predicted = model.output(features,false);

                eval.eval(labels, predicted);
            }

            System.out.println(eval.stats());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
