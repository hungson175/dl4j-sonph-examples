package org.deeplearning4j.examples.sonph.examples.adtima;


import org.junit.Assert;

import java.util.ArrayList;
import java.util.Arrays;

public class SVMVector {
    private static final double EPS = 1e-9;
    private final int label;
    private ArrayList<Double> values;

    public String toLibSVMLine() {
        StringBuffer sb = new StringBuffer();
        sb.append(label);
        for (int i = 0; i < values.size(); i++) {
            if ( values.get(i) > EPS ) {
                sb.append(" ").append(i).append(":").append(values.get(i));
            }
        }
        return sb.toString();
    }

    public void removeColumn(int index) {
        Assert.assertTrue(index >= 0 && index < values.size());
        values.remove(index);
    }

    /**
     *
     * Encode the integer column got values in range [lowerBound, upperBound] (incl.)
     * @param index
     * @param lowerBound inclusive
     * @param upperBound inclusive
     *
     */
    public void oneHotEncodeIntegerColumn(int index, int lowerBound, int upperBound) {
        int lenBefore = values.size();
        int valueAtIndex = (int) Math.round(values.get(index));
        Assert.assertEquals(values.get(index), (double) valueAtIndex, EPS);
        values.set(index, 0.0);
        ArrayList<Double> addMore = new ArrayList<>(upperBound-lowerBound);
        for (int i = 0; i < (upperBound - lowerBound); i++) {
            addMore.add(0.0);
        }
        values.addAll(index, addMore);
        values.set(index+(valueAtIndex-lowerBound),1.0);
        int lenAfter = values.size();
        Assert.assertEquals(lenBefore + (upperBound - lowerBound), lenAfter); //minus 1 for the existing column converted to one-hot
    }

    public int size() {
        return values.size();
    }

    public void setValue(int j, double v) {
        this.values.set(j, v);
    }

    public static class Builder {
        private final double[] values;
        private int label;

        public Builder(int nFeatures) {
            values = new double[nFeatures];
        }

        public void addLabel(int label) {
            this.label = label;
        }

        public void addPair(Integer index, Double value) {
            values[index] = value;
        }

        public SVMVector toVector() {
            return new SVMVector(label, values);
        }
    }


    private SVMVector(int label, double[] values) {
        this.label = label;
        this.values = new ArrayList<>(values.length);
        for (double value : values) {
            this.values.add(value);
        }


    }

    public int getLabel() {
        return label;
    }

    public double getValue(int index) {
        return values.get(index);
    }

    public static SVMVector parseLine(String line, int nFeatures) {
        String[] words = line.split(" ");
        Builder vb = new SVMVector.Builder(nFeatures);
        vb.addLabel(Integer.parseInt(words[0]));
        for (int i = 1; i < words.length; i++) {
            String ps = words[i];
            String[] temp = ps.split(":");
            Assert.assertEquals(2, temp.length);
            Integer index = Integer.parseInt(temp[0]);
            Double value = Double.parseDouble(temp[1]);
            vb.addPair(index, value);
        }
        return vb.toVector();
    }
}
