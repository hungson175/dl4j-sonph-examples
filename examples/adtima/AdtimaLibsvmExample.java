package org.deeplearning4j.examples.sonph.examples.adtima;

import org.datavec.api.conf.Configuration;
import org.datavec.api.records.reader.impl.misc.SVMLightRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.examples.sonph.examples.ModelWriter;
import org.deeplearning4j.examples.sonph.examples.MyScoreIterationListener;
import org.deeplearning4j.examples.sonph.examples.iris.Helpers;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.parallelism.ParallelWrapper;
import org.nd4j.jita.conf.CudaEnvironment;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.buffer.DataBuffer;
import org.nd4j.linalg.api.buffer.util.DataTypeUtil;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.IOException;

public class AdtimaLibsvmExample {

    private static final double LEARNING_RATE = 0.05;
//    public static final double MOMENTUM = 0.9;

    private static final int N_LAYERS_COUNT = 4;
    private static final int N_NODES_EACH_LAYER = 64;
    private static final int N_EPOCHS = 100000;

    static int[] N_HIDDEN_LAYERS = new int[N_LAYERS_COUNT]; //TODO: remove this
    static {
        for (int i = 0; i < N_LAYERS_COUNT; i++) {
            N_HIDDEN_LAYERS[i] = N_NODES_EACH_LAYER;
        }
//        N_HIDDEN_LAYERS[N_LAYERS_COUNT] = N_NODES_EACH_LAYER / 2; //TODO: remove this
//        N_HIDDEN_LAYERS[N_LAYERS_COUNT+1] = N_NODES_EACH_LAYER / 4; //TODO: remove this

    }

    private static final int BATCH_SIZE = 1024;

    public static final int PERIOD_TO_SAVE = 10000;
    public static final int PRINT_ITERATIONS = 100;


    private static final int N_INPUTS = 44;

    private static final int N_CLASSES = 2;
    private static final long SEED = 1;

    private static final String TRAIN_FILE = "/home/hungson175/Data/Adtima/train_13_170718_processed.libsvm";
//    private static final String DEV_FILE = "/home/hungson175/Data/Adtima/valid_13_170718_staging.libsvm";
    private static final String CONFIG_STRING = "LR:" + LEARNING_RATE + "_BATCH:" + BATCH_SIZE + "_L:" + N_LAYERS_COUNT + "_NODES:" + N_NODES_EACH_LAYER;
    public static final String MODEL_BASE_DIR = "/media/hungson175/DataStorage/Data/Adtima";


    public static void main(String[] args) throws IOException, InterruptedException {

        // PLEASE NOTE: For CUDA FP16 precision support is available
//        Nd4j.setDataType(DataBuffer.Type.HALF);
//
//        // temp workaround for backend initialization
//
//        CudaEnvironment.getInstance().getConfiguration()
//            // key option enabled
//            .allowMultiGPU(true)
//
//            // we're allowing larger memory caches
//            .setMaximumDeviceCache(2L * 1024L * 1024L * 1024L)
//
//            // cross-device access is used for faster model averaging over pcie
//            .allowCrossDeviceAccess(true);
//
        DataTypeUtil.setDTypeForContext(DataBuffer.Type.HALF);
        CudaEnvironment.getInstance().getConfiguration().allowMultiGPU(true);
        CudaEnvironment.getInstance().getConfiguration()
            .setMaximumDeviceCacheableLength(4L*1024 * 1024 * 1024L)
            .setMaximumDeviceCache(6L * 1024 * 1024 * 1024L)
            .setMaximumHostCacheableLength(4L*1024 * 1024 * 1024L)
            .setMaximumHostCache(6L * 1024 * 1024 * 1024L);

        new AdtimaLibsvmExample().start();
    }

    private void start() throws IOException, InterruptedException {



        Configuration config = new Configuration();
        config.setBoolean(SVMLightRecordReader.ZERO_BASED_INDEXING, true);
        config.setInt(SVMLightRecordReader.NUM_FEATURES, N_INPUTS);

        SVMLightRecordReader trainReader = new SVMLightRecordReader();
        trainReader.initialize(config, new FileSplit(new File(TRAIN_FILE)));
        DataSetIterator trainIterator = new RecordReaderDataSetIterator(trainReader, BATCH_SIZE, N_INPUTS, N_CLASSES);

//        SVMLightRecordReader devReader = new SVMLightRecordReader();
//        devReader.initialize(config, new FileSplit(new File(DEV_FILE)));
//        DataSetIterator devIterator = new RecordReaderDataSetIterator(devReader, BATCH_SIZE, N_INPUTS, N_CLASSES);

        MultiLayerConfiguration conf = Helpers.createNN(
            N_INPUTS, N_CLASSES, N_HIDDEN_LAYERS,
//            new Nesterovs(LEARNING_RATE, MOMENTUM),
            new Adam(LEARNING_RATE),
            0.01,
            LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD,
            Activation.SOFTMAX,
            SEED);

        long startTime = System.currentTimeMillis();

        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();
        model.setListeners(
            new MyScoreIterationListener(PRINT_ITERATIONS),
            new ModelWriter(PERIOD_TO_SAVE, MODEL_BASE_DIR, "model_N"+N_NODES_EACH_LAYER+"xL"+N_LAYERS_COUNT+"xB"+BATCH_SIZE+"xLR"+LEARNING_RATE)

        );

//        ParallelWrapper wrapper = new ParallelWrapper.Builder(model)
//            // DataSets prefetching options. Set this value with respect to number of actual devices
//            .prefetchBuffer(24)
//            // set number of workers equal or higher then number of available devices. x1-x2 are good values to start with
//            .workers(2)
//            // rare averaging improves performance, but might reduce model accuracy
//            .averagingFrequency(3)
//            // if set to TRUE, on every averaging model score will be reported
////            .reportScoreAfterAveraging(true)
//
//            .build();
        System.out.println("Toal epochs: " + N_EPOCHS);
        for ( int n = 0; n < N_EPOCHS; n++) {
            model.fit( trainIterator );
//            wrapper.fit( trainIterator );
            System.out.println(CONFIG_STRING);
            System.out.println("Runtime until n-th epoch done: " + ((System.currentTimeMillis()  - startTime) / 1000.0) + " seconds");
        }

//        System.out.println("Evaluate model.... "+CONFIG_STRING);
//        Evaluation eval = new Evaluation(N_OUTPUTS);
//        while(devIterator.hasNext()){
//            DataSet t = devIterator.next();
//            INDArray features = t.getFeatureMatrix();
//            INDArray labels = t.getLabels();
//            INDArray predicted = model.output(features,false);
//
//            eval.eval(labels, predicted);
//        }
//
//        LossNegativeLogLikelihood lossNegativeLogLikelihood = new LossNegativeLogLikelihood();
//
//
//
//        System.out.println(eval.stats());
//        System.out.println("CONFIG: " + CONFIG_STRING);
//        System.out.println("Total runtime: " + ((System.currentTimeMillis() - startTime)/1000.0) + " s");
//

    }
}
