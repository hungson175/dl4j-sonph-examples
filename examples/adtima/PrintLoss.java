package org.deeplearning4j.examples.sonph.examples.adtima;

import org.datavec.api.conf.Configuration;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.records.reader.impl.misc.SVMLightRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.examples.sonph.examples.Helpers;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.io.*;
import java.util.ArrayList;

/**
 * ONE-HOT Time
 *
 * N32xL4xB1024xLR0.1
 * model_N32xL4xB1024xLR0.1_100000      -> 0.011798136803529462
 * model_N32xL4xB1024xLR0.1_210000      -> 0.0121439112606248
 * model_N32xL4xB1024xLR0.1_220000      -> 0.01158226077478899
 *
 * N64xL4xB1024xLR0.1
 * model_N64xL4xB1024xLR0.1_90000       -> 0.011671817180986477
 * model_N64xL4xB1024xLR0.1_120000      -> 0.011598225000061106
 * model_N64xL4xB1024xLR0.1_170000      -> 0.011546867120802573
 * model_N64xL4xB1024xLR0.1_1770000     -> 0.011605977055432839
 * model_N64xL4xB1024xLR0.1_1700000     -> 0.011469098382637507
 * model_N64xL4xB1024xLR0.1_3000000     -> 0.01147294319205936
 * model_N64xL4xB1024xLR0.1_3020000     -> 0.011509104354304488
 *
 *
 *
 * N64xL8xB1024xLR0.1
 * model_N64xL8xB1024xLR0.1_60000       -> 0.011891427534054675
 * model_N64xL8xB1024xLR0.1_80000       -> 0.0117990325762864
 * model_N64xL8xB1024xLR0.1_110000      -> 0.011894451846788659
 * model_N64xL8xB1024xLR0.1_2240000     -> 0.01192618351029941
 *
 * N64xL32xB1024xLR0.1
 * model_N64xL32xB1024xLR0.1_300000     -> 0.011853914581845735
 *
 * N2048xL2xB1024xLR0.1
 * model_N2048xL2xB1024xLR0.1_10000     -> 0.01348012239607614
 * model_N2048xL2xB1024xLR0.1_20000     -> 0.012514691757129947
 * model_N2048xL2xB1024xLR0.1_40000     -> 0.012207396132604097
 * model_N2048xL2xB1024xLR0.1_900000    -> 0.011575358678623015
 * model_N2048xL2xB1024xLR0.1_1850000   -> 0.011588053768262607
 *
 * N1xL0xB1024xLR0.1
 * model_N1xL0xB1024xLR0.1_1900000      -> 0.01166378524351604
 * model_N1xL0xB1024xLR0.1_3430000      -> 0.011665861278309167
 * ====================================
 * Not one-hot hour yet
 * N32xL4xB1024xLR0.1 not bad, but also very little improvement (100k -> 3.6M)
 * model_N32xL4xB1024xLR0.1_100000  -> 0.011890746422435261
 * model_N32xL4xB1024xLR0.1_200000  -> 0.011566497736142597
 * model_N32xL4xB1024xLR0.1_500000  -> 0.01158478144338901
 * model_N32xL4xB1024xLR0.1_1000000  -> 0.011525037512639493
 * model_N32xL4xB1024xLR0.1_2000000  -> 0.011521707401665538
 * model_N32xL4xB1024xLR0.1_3000000  -> 0.01153308480530371
 * model_N32xL4xB1024xLR0.1_3660000 -> 0.011499608734089591 (not bad)
 *
 * N32xL16xB64xLR0.1 bad (100k vs 2M)
 * model_N32xL16xB64xLR0.1_100000  -> 0.012615881763478078
 * model_N32xL16xB64xLR0.1_2000000 -> 0.0124397030772123
 *
 * N256xL2xB64xLR0.05 Almost no improvement at all ! (100k vs 7M iterations)
 * model_N256xL2xB64xLR0.05_100000 -> 01212462460077391
 * model_N256xL2xB64xLR0.05_7730000 -> .012027365145544072
 *
 */
public class PrintLoss {

    private static final String BASE_MODEL_DIR = "/media/hungson175/DataStorage/Data/Adtima";
    private static final String MODEL_FILE = "model_N64xL4xB1024xLR0.05_100000 .model";
    public static final String BASE_DIR = "/home/hungson175/Data/Adtima";
//    public static final String DEV_FILE = "valid_13_170718_staging.libsvm";
    public static final String DEV_FILE = "valid_13_170718_processed.libsvm"; //011496027232940598
//    public static final String DEV_FILE = "train_13_170718_processed.libsvm"; //010402436001021193
    private static final String DEV_FILE_PATH = BASE_DIR +"/"+ DEV_FILE;
    private static final int BATCH_SIZE = 4096;
    private static final String RESULT_FILE_PATH = BASE_DIR+"/"+"dev_result.txt";
//    private static final int N_INPUTS = 21;
    private static final int N_INPUTS = 44;
    private static final int N_CLASSES = 2;
    private static final int REPORT_PERIOD = 10000;

    /**
     * - Load model from disk
     * - Read the dev file
     * - make prediction for each row, then write to dev_result file
     * - Read the file and compute the logloss
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        MultiLayerNetwork model = Helpers.readModel(BASE_MODEL_DIR + "/" + MODEL_FILE);

        Configuration config = new Configuration();
        config.setBoolean(SVMLightRecordReader.ZERO_BASED_INDEXING, true);
        config.setInt(SVMLightRecordReader.NUM_FEATURES, N_INPUTS);


        SVMLightRecordReader devReader = new SVMLightRecordReader();
        devReader.initialize(config, new FileSplit(new File(DEV_FILE_PATH)));
        DataSetIterator devIterator = new RecordReaderDataSetIterator(devReader, BATCH_SIZE, N_INPUTS, N_CLASSES);

        try (PrintWriter printWriter = new PrintWriter(new FileWriter( RESULT_FILE_PATH), false)) {
            long startTime = System.currentTimeMillis();
            int index = 0 , lastIndex = 0;
            while ( devIterator.hasNext()) {
                DataSet dataSet = devIterator.next();
                INDArray featureMatrix = dataSet.getFeatureMatrix();
                INDArray labels = dataSet.getLabels();
                INDArray output = model.output(featureMatrix);
                for (int i = 0; i < output.size(0); i++) {
                    double clickProb = output.getDouble(i, 1);
                    printWriter.println(labels.getInt(i, 1)+" "+clickProb ) ;
                    index++;
                }
                if ( index - lastIndex > REPORT_PERIOD) {
                    lastIndex = index;
                    System.out.println("Current index: " + index + ", elapsed time: " + ((System.currentTimeMillis() - startTime) / 1000) + " s");
                }
            }
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(RESULT_FILE_PATH))) {
            String line = null;
            ArrayList<Integer> labels = new ArrayList<>();
            ArrayList<Double> prob = new ArrayList<>();
            while ( (line = reader.readLine()) != null) {
                if ( line.trim().length() == 0) continue;
                String[] words = line.split(" ");
                Integer label = Integer.parseInt(words[0]);
                Double p = Double.parseDouble(words[1]);
                labels.add(label);
                prob.add(p);
            }

            double loss = Helpers.logLoss(prob, labels);
            System.out.println("Logloss: " + loss);
        }

    }
}
