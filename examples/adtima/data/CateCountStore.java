package org.deeplearning4j.examples.sonph.examples.adtima.data;

import org.deeplearning4j.examples.sonph.examples.Helpers;
import redis.clients.jedis.*;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

enum  CateCountStore {
    INSTANCE;
    public static final String HOST = "localhost";
    public static final int PORT = 59153;

    public static final String[] CATEGORIES = {
        "Laodong-Vieclam",
        "Chungkhoan",
        "Phapluat",
        "Bongda",
        "Taichinh",
        "Xeco",
        "Thoisu-Xahoi",
        "Tinhyeugioitinh",
        "Thoisu",
        "Tinvideo",
        "Daotao-Thicu",
        "Xahoi",
        "Vanhoa",
        "Anninh-Trattu",
        "Khonggian-Kientruc",
        "Lamdep",
        "Suckhoe-Yte",
        "Amnhac",
        "Hinhsu-Dansu",
        "Dinhduong-Lamdep",
        "Moitruong-Khihau",
        "Games",
        "Thethao",
        "CNTT-Vienthong",
        "Kinhdoanh",
        "Doisong",
        "Tinmoi",
        "Quanvot",
        "Tinhyeu-Gioitinh",
        "Tinnong",
        "Giaitri",
        "Thegioi",
        "Giaoduc",
        "Giaothong",
        "Kinhte",
        "Quanly-Quyhoach",
        "Hinhsu",
        "Suckhoe",
        "Khoahoc-Tunhien",
        "Thoitrang",
        "Amthuc",
        "Nhadat",
        "Tinhyeu-Honnhan",
        "Khoahoc-Congnghe",
        "Thietbi-Phancung",
        "Nghethuat",
        "Dienanh-Truyenhinh",
        "Hocbong-Duhoc",
        "Dulich"
    };

    public static final String[] DAYS_IN_WEEK = {
        "MON",
        "TUE",
        "WED",
        "THU",
        "FRI",
        "SAT",
        "SUN"
    };

    public static final String[] PERIODS_IN_DAY = {
        "MORNING",
        "AFTERNOON",
        "EVENING"
    };

    private static final JedisPoolConfig poolConfig = buildPoolConfig();
    private static final int N_SCAN_BATCH = 1024;
    private static JedisPool jedisPool = new JedisPool(poolConfig, HOST, PORT);

    private static JedisPoolConfig buildPoolConfig() {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(128);
        poolConfig.setMaxIdle(128);
        poolConfig.setMinIdle(16);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);
        return poolConfig;
    }

//    public static void main(String[] args) throws IOException {
//        new RedisStore().start();
//    }

    public HashMap<Long, HashMap<String, Integer>> getUid2CateCount() throws IOException {
        try  (Jedis jedis = jedisPool.getResource()) {
            ScanParams params = new ScanParams();
            params.count(N_SCAN_BATCH);
            String cursor = "0";
            int cnt = 0;
            HashMap<Long, HashMap<String, Integer>> uid2CateCount = new HashMap<>();
            long startTime = System.currentTimeMillis();
            while (true) {
                ScanResult<String> scanResult = jedis.scan(cursor , params);
                cursor = scanResult.getStringCursor();
                if ( cursor.equals("0")) break;
                List<String> uids = scanResult.getResult();
                HashMap<Long, HashMap<String, Integer>> temp = getCount(jedis,uids);
                uid2CateCount.putAll(temp);
                cnt++;
                if ( cnt % 10 == 0 ) {
                    System.out.println("Count : " + cnt + " at appr.: "  + (cnt * N_SCAN_BATCH)  + " elapsed time: " + (System.currentTimeMillis() - startTime));
                }
            }
            return uid2CateCount;
        }
    }

    private HashMap<Long, HashMap<String, Integer>> getCount(Jedis jedis, List<String> uids) throws IOException {
        Pipeline ppl = jedis.pipelined();
        for (String uid : uids) {
            ppl.hgetAll(uid);
        }
        HashMap<Long, HashMap<String, Integer>> results = new HashMap<>();
        List<Object> list = ppl.syncAndReturnAll();
        Iterator<String> uidIter = uids.iterator();
        list.forEach(obj -> {
            HashMap<String, String> mapRS = (HashMap<String, String>) obj;
            HashMap<String, Integer> mapCount = new HashMap<>();
            mapRS.keySet().forEach( key -> mapCount.put(key, Integer.parseInt(mapRS.get(key))));
            String uid = uidIter.next();
            Long longId = Helpers.longHashId(uid);
            results.put(longId, mapCount);
        });
        ppl.close();

        return results;
    }
}
