package org.deeplearning4j.examples.sonph.examples.adtima;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.datavec.api.conf.Configuration;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.records.reader.impl.misc.SVMLightRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.transform.TransformProcess;
import org.datavec.api.transform.schema.Schema;
import org.datavec.api.writable.Writable;
import org.datavec.spark.transform.SparkTransformExecutor;
import org.datavec.spark.transform.misc.StringToWritablesFunction;
import org.datavec.spark.transform.misc.WritablesToStringFunction;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TransformVuLALibsvm {
    private static final int N_INPUTS = 21;
//    private static final String TRAIN_FILE_PATH = "/home/hungson175/Data/Adtima/train_13_170718_staging.libsvm";
//    private static final String PROCESSED_FILE_PATH = "/home/hungson175/Data/Adtima/train_13_170718_processed.libsvm";
    private static final String TRAIN_FILE_PATH = "/home/hungson175/Data/Adtima/valid_13_170718_staging.libsvm";
    private static final String PROCESSED_FILE_PATH = "/home/hungson175/Data/Adtima/valid_13_170718_processed.libsvm";
//    private static final String TRAIN_FILE = "/home/hungson175/Data/Other/ownsample.libsvm";
//    private static final String PROCESSED_FILE = "/home/hungson175/Data/Other/ownprocessed.libsvm";
    private static final int BATCH_SIZE = 2;
    private static final int N_CLASSES = 2;
    public static final int REPORT_PERIOD = 100000;
    private static final double EPS = 1e-9;
    private JavaSparkContext sc;


    public static void main(String[] args) throws IOException, InterruptedException {
        /**
         * What to do ?
         * 0. Read & transform the sample file first
         * 1. Read the file
         * 2. One hot encode the 24-hour data
         * 3. remove 12 hour data
         * 4. Validate the output file by eye-balling
         */
        new TransformVuLALibsvm().start();

    }

    /**
     *
     * Train set
     * Done computing mean = 1150.000000 after 0
     * Done computing dev = 0.197958 after 0
     *
     * Valid set
     * Done computing mean = 444.000000 after 0
     * Done computing dev = 1.284750 after 0
     */


    private void start() throws IOException, InterruptedException {
        try (
            BufferedReader reader = new BufferedReader(new FileReader(TRAIN_FILE_PATH));
            PrintWriter printer = new PrintWriter(new FileWriter(PROCESSED_FILE_PATH));
            )
        {
            String line = null;
            int count = 0;
            long startTime = System.currentTimeMillis();

            ArrayList<SVMVector> rows = new ArrayList<>();
            while (  ( line = reader.readLine())  != null ) {
                count++;
//                System.out.println("Original line: ["+line+"]");
                SVMVector vector = SVMVector.parseLine(line,21);
//                System.out.println("Vector line: ["+vector.toLibSVMLine()+"]");
                vector.removeColumn(13);
                vector.oneHotEncodeIntegerColumn(12,0,23);
//                String processedLine = vector.toLibSVMLine();
//                printer.println(processedLine);
//                System.out.println("Processed line: ["+processedLine+"]");
//                System.out.println("========================");
                rows.add(vector);

                if ( count % REPORT_PERIOD == 0 )
                    System.out.printf("Processed %d lines in %d seconds\n",count,(System.currentTimeMillis() - startTime) / 1000 );
            }

//            normalizeData(rows, 39);
            scaleData(rows, 39, 1150, 0.197958 );
            rows.forEach(row -> printer.println(row.toLibSVMLine()));
        }
    }

    private void scaleData(ArrayList<SVMVector> rows, int normalizingColIndex, double mean, double dev) {
        for (int i = 0; i < rows.size(); i++) {
            double current = rows.get(i).getValue(normalizingColIndex);
            rows.get(i).setValue(normalizingColIndex, (current - mean)/(dev+EPS) );
        }
    }


    private void normalizeData(ArrayList<SVMVector> rows, int normalizinngColIndex) {
        long startTime = System.currentTimeMillis();
        long startNormalize = startTime;
        SVMVector vector = rows.get(0);
        int nFeatures = vector.size();
        double meanJCol = 0.0;
        for (int i = 0; i < rows.size(); i++) {
            meanJCol += vector.getValue(normalizinngColIndex);
        }
        meanJCol /= rows.size();
        System.out.printf("Done computing mean = %f after %d\n",meanJCol,((System.currentTimeMillis() - startTime) / 1000));


        startTime = System.currentTimeMillis();
        double standardDev = 0.0;

        for (int i = 0; i < rows.size(); i++) {
            double diff = rows.get(i).getValue(normalizinngColIndex)  - meanJCol;
            standardDev += diff * diff;
        }
        standardDev = Math.sqrt(standardDev) / ( rows.size());

        System.out.printf("Done computing dev = %f after %d\n",standardDev,((System.currentTimeMillis() - startTime) / 1000));

        startTime = System.currentTimeMillis();
//        for (int i = 0; i < rows.size(); i++) {
//            double current = rows.get(i).getValue(normalizinngColIndex);
//            rows.get(i).setValue(normalizinngColIndex, (current - meanJCol)/(standardDev+EPS) );
//
//        }
        scaleData(rows, normalizinngColIndex, meanJCol, standardDev);
        System.out.println("Done computing normalize after: " + ((System.currentTimeMillis() - startNormalize)/1000) );
//        long startTime = System.currentTimeMillis();
//        long startNormalize = startTime;
//        SVMVector vector = rows.get(0);
//        int nFeatures = vector.size();
//        double[] meanJCol = new double[nFeatures];
//        for (int i = 0; i < rows.size(); i++) {
//            for (int j = 0; j < nFeatures; j++) {
//                meanJCol[j] += vector.getValue(j);
//            }
//        }
//        for (int j = 0; j < nFeatures; j++) {
//            meanJCol[j] /= rows.size();
//        }
//        System.out.println("Done computing mean after: " + ((System.currentTimeMillis() - startTime) / 1000));
//
//
//        startTime = System.currentTimeMillis();
//        double[] standardDev = new double[nFeatures];
//
//        for (int i = 0; i < rows.size(); i++) {
//            for (int j = 0; j < nFeatures; j++) {
//                double diff = rows.get(i).getValue(j)  - meanJCol[j];
//                standardDev[j] += diff * diff;
//            }
//        }
//
//        for (int j = 0; j < nFeatures; j++) {
//            standardDev[j] = Math.sqrt(standardDev[j]) / ( rows.size());
//        }
//        System.out.println("Done computing dev after: " + ((System.currentTimeMillis() - startTime) / 1000));
//
//        startTime = System.currentTimeMillis();
//        for (int i = 0; i < rows.size(); i++) {
//            for (int j = 0; j < nFeatures; j++) {
//                double current = rows.get(i).getValue(j);
//                rows.get(i).setValue(j, (current - meanJCol[j])/(standardDev[j]+EPS) );
//            }
//        }
//
//        System.out.println("Done computing normalize after: " + ((System.currentTimeMillis() - startNormalize)/1000) );
    }

}
