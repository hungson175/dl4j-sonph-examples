package org.deeplearning4j.examples.sonph.examples;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.optimize.api.BaseTrainingListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

public class MyScoreIterationListener extends BaseTrainingListener implements Serializable {
    private static final Logger log = LoggerFactory.getLogger(org.deeplearning4j.optimize.listeners.ScoreIterationListener.class);
    private int printIterations = 10;
    private long startTime = 0;

    public MyScoreIterationListener(int printIterations) {
        this.printIterations = printIterations;
        this.startTime = System.currentTimeMillis();
    }

    public void startTimer() {
        this.startTime = System.currentTimeMillis();
    }

    public void iterationDone(Model model, int iteration, int epoch) {
        if (this.printIterations <= 0) {
            this.printIterations = 1;
        }

        if (iteration % this.printIterations == 0) {
            double score = model.score();
            log.info("Score at epoch#iteration {}#{} is {}, elapsed time {} s", epoch, iteration, score, (System.currentTimeMillis() - startTime )  / 1000);
        }

    }
}
