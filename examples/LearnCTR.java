package org.deeplearning4j.examples.sonph.examples;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.transform.ReduceOp;
import org.datavec.api.transform.TransformProcess;
import org.datavec.api.transform.reduce.Reducer;
import org.datavec.api.transform.schema.Schema;
import org.datavec.api.writable.Writable;
import org.datavec.spark.transform.SparkTransformExecutor;
import org.datavec.spark.transform.misc.StringToWritablesFunction;
import org.datavec.spark.transform.misc.WritablesToStringFunction;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class LearnCTR {
    private static final String DATA_FILE = "/home/hungson175/Data/Other/LearnCTR.csv";
    private static final String PROCESSED_FILE = "/home/hungson175/Data/Other/LearnCTR_processed.csv";
    private static JavaSparkContext sc;

    public static void main(String[] args) throws IOException {
        /**
         * A: 0.33 (33%)
         * B: 0
         * C: 0.5
         */
        Schema inputSchema = new Schema.Builder()
            .addColumnsString("cate")
            .addColumnInteger("click").build();

        TransformProcess tp = new TransformProcess.Builder(inputSchema)
            .duplicateColumn("cate","cate_temp")
            .reduce(
                new Reducer.Builder(ReduceOp.Count)
                    .keyColumns("cate")
                    .sumColumns("impression")
                    .build()
            )
            .reduce( new Reducer.Builder(ReduceOp.Sum)
                .keyColumns("cate_temp")
                .sumColumns("click")
                .build()
            )
            .build();

        SparkConf sparkConf = new SparkConf();
        sparkConf.setMaster("local[*]");
        sparkConf.setAppName("Avazu Record Reader Transform");
        sc = new JavaSparkContext(sparkConf);

        JavaRDD<String> lines = sc.textFile(DATA_FILE);
        // convert to Writable
        JavaRDD<List<Writable>> samples = lines.map(new StringToWritablesFunction(new CSVRecordReader()));
        // run our transform process
        JavaRDD<List<Writable>> processed = SparkTransformExecutor.execute(samples,tp);
        // convert Writable back to string for export
        JavaRDD<String> toSave= processed.map(new WritablesToStringFunction(","));

        try (PrintWriter printWriter = new PrintWriter(new FileWriter(PROCESSED_FILE))) {
            List<String> list = toSave.collect();
            list.forEach(line -> printWriter.println(line));
        }


    }
}
