package org.deeplearning4j.examples.sonph.examples.avazu;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.examples.sonph.examples.Helpers;
import org.deeplearning4j.examples.userInterface.util.DistributionEmperical;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
//import org.nd4j.jita.conf.CudaEnvironment;
import org.nd4j.linalg.api.buffer.DataBuffer;
import org.nd4j.linalg.api.buffer.util.DataTypeUtil;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Performance:
 * GPU
 * - 16384 x 1: 9000s
 *
 * CPU
 * - 256 x 10: 600s
 */
public class PrintSubmission {
    static final String BASE_DIR = "/home/hungson175/Data/Avazu";
    //static final String BASE_DIR = "/media/hungson175/DataStorage/Data/Avazu";
    static final String MODEL_FILE_NAME = "model_N4096xL2xB512_169000.model";
    static final String TEST_FILE_NAME = "test_merged.csv";
    static final String IDS_FILE_NAME = "ids.txt";
    static final String SUBMISSION_FILE_NAME = "submission.csv";


    static final String FULL_MODEL_FILE_PATH = BASE_DIR + "/" + MODEL_FILE_NAME;
    static final String FULL_TEST_FILE_PATH = BASE_DIR + "/" + TEST_FILE_NAME;
    static final String FULL_IDS_FILE_PATH = BASE_DIR + "/" + IDS_FILE_NAME;
    private static final String FULL_SUBMISSION_FILE_PATH = BASE_DIR + "/" + SUBMISSION_FILE_NAME;

    private static final int BATCH_SIZE = 16384;
    private static final int REPORT_PERIOD = 1000;

    private ArrayList<Double> prob;


    public static void main(String[] args) throws IOException, InterruptedException {
//        CudaEnvironment.getInstance().getConfiguration().allowMultiGPU(true);
//        DataTypeUtil.setDTypeForContext(DataBuffer.Type.HALF);
//        CudaEnvironment.getInstance().getConfiguration()
//            .setMaximumDeviceCacheableLength(1024 * 1024 * 1024L)
//            .setMaximumDeviceCache(6L * 1024 * 1024 * 1024L)
//            .setMaximumHostCacheableLength(1024 * 1024 * 1024L)
//            .setMaximumHostCache(6L * 1024 * 1024 * 1024L);
        new PrintSubmission().start();
    }

    private void start() throws IOException, InterruptedException {

        long startTime = System.currentTimeMillis();
        MultiLayerNetwork model = Helpers.readModel(FULL_MODEL_FILE_PATH);
        System.out.println("Loaded model in: " + (System.currentTimeMillis() - startTime) + " ms");


        RecordReader testReader = new CSVRecordReader();
        testReader.initialize(new FileSplit(new File(FULL_TEST_FILE_PATH)));
        DataSetIterator testIter = new RecordReaderDataSetIterator(testReader, BATCH_SIZE);



        try (PrintWriter printWriter = new PrintWriter(new FileWriter(FULL_SUBMISSION_FILE_PATH, false))) {

            startTime = System.currentTimeMillis();
            ArrayList<String> ids = Helpers.readLines(FULL_IDS_FILE_PATH);
            System.out.println("Read ids: " + (System.currentTimeMillis() - startTime) + " ms");
            //print header
            printWriter.println("id,click");
            startTime = System.currentTimeMillis();
            int index = 0;
            int lastIndex = 0;
            while (testIter.hasNext()) {
                DataSet t = testIter.next();
                INDArray featureMatrix = t.getFeatureMatrix();
                INDArray output = model.output(featureMatrix);
                for (int i = 0; i < output.size(0); i++) {
                    double clickProb = output.getDouble(i, 1);
                    printWriter.println(ids.get(index)+","+clickProb);
                    index++;
                }
                if ( index - lastIndex > REPORT_PERIOD) {
                    lastIndex = index;
                    System.out.println("Current index: " + index + ", elapsed time: " + ((System.currentTimeMillis() - startTime) / 1000) + " s");
                }
            }

        }

    }
}
