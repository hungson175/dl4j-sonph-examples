package org.deeplearning4j.examples.sonph.examples.avazu;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.jcodec.common.Assert;

import java.io.*;
import java.util.HashMap;


public class EncodeCategoryCTR {
    static final String BASE_DIR = "/home/hungson175/Data/Avazu";
    protected static final String TRAIN_FILE = BASE_DIR + "/" + "train.csv";
    protected static final String COUNT_RESULTS_FILE = BASE_DIR + "/" + "count_fields.txt";

    public static final String[] ALL_FIELDS = {"id", "click", "hour", "C1", "banner_pos",
        "site_id", "site_domain", "site_category",
        "app_id", "app_domain", "app_category",
        "device_id", "device_ip", "device_model", "device_type", "device_conn_type",
        "C14", "C15", "C16", "C17", "C18", "C19", "C20", "C21"};
    protected  static final String[] FIELDS_TO_COUNT = new String[]{
        "site_id","site_domain",
        "app_id","app_domain",
        "device_model",
        "C14","C17","C20"};

    public static void main(String[] args) throws IOException {
        new EncodeCategoryCTR().start();
    }

    private void start() throws IOException {
        /**
         * Notes: what about device_id, device_ip ?
         * site_id: 4737
         * site_domain: 7745
         * app_id: 8552
         * app_domain: 559
         * device_model: 8251
         * C14: 2626
         * C17: 435
         * C20: 172
         */


        /**
         * Read all lines
         * - store count_impr(site_id) , count_click(site_id)
         */

        Reader in = new FileReader(TRAIN_FILE);

        Iterable<CSVRecord> records = CSVFormat.RFC4180.withHeader(ALL_FIELDS).withSkipHeaderRecord().parse(in);


        HashMap<String, HashMap<String, Integer>> countClick = new HashMap<>();
        HashMap<String, HashMap<String, Integer>> countImp = new HashMap<>();



        for (String field : FIELDS_TO_COUNT) {
            countClick.put(field, new HashMap<>());
            countImp.put(field, new HashMap<>());
        }


        int lineCount = 0;
        long startTime = System.currentTimeMillis();
        for (CSVRecord record : records) {
            lineCount ++;
            int isClick = Integer.parseInt(record.get("click"));
            for (String field : FIELDS_TO_COUNT) {
                storeCount(countClick.get(field), countImp.get(field), record, field, isClick);
            }
            if ( lineCount % 10000 == 0 ) {
                System.out.printf("Processed %d lines, runtime %d s\n", lineCount, (System.currentTimeMillis() - startTime) / 1000);
            }
        }
        System.out.printf("Done processed %d lines in %d seconds\n", lineCount, (System.currentTimeMillis() - startTime) / 1000);


        System.out.printf("Printing results to file %s...\n",COUNT_RESULTS_FILE);
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(COUNT_RESULTS_FILE))) {
            for (String field : FIELDS_TO_COUNT) {
                HashMap<String, Integer> mapImp = countImp.get(field);
                HashMap<String, Integer> mapClick = countClick.get(field);

                Assert.assertTrue(mapImp.keySet().size() >= mapClick.keySet().size());
                printWriter.printf("%s %d\n",field,mapImp.keySet().size());
                for (String key : mapImp.keySet()) {
                    Integer cntClick = mapClick.get(key);
                    if ( cntClick == null) cntClick = Integer.valueOf(0);
                    Integer cntImp = mapImp.get(key);
                    Assert.assertNotNull(cntImp);
                    printWriter.printf("%s %d %d\n",key, cntClick, cntImp);
                }
            }
        }


    }

    private void storeCount(
        HashMap<String, Integer> countClick,
        HashMap<String, Integer> countImp,
        CSVRecord record,
        String field,
        int isClick) {
        String key = record.get(field);
        if ( isClick == 1)
            incMap(countClick, key);
        incMap(countImp, key);
    }

    private void incMap(HashMap<String, Integer> mapCount, String key) {
        Integer value = mapCount.get(key);
        if ( value == null) value = Integer.valueOf(0);
        mapCount.put(key, value + 1);
    }
}
