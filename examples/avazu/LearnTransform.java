package org.deeplearning4j.examples.sonph.examples.avazu;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.transform.TransformProcess;
import org.datavec.api.transform.schema.Schema;
import org.datavec.api.writable.Writable;
import org.datavec.spark.transform.SparkTransformExecutor;
import org.datavec.spark.transform.misc.StringToWritablesFunction;
import org.datavec.spark.transform.misc.WritablesToStringFunction;
import org.junit.Assert;

import java.util.List;

public class LearnTransform {
    final static String DATA_FILE = "/home/hungson175/Data/Other/Fiction.csv";
    private static JavaSparkContext sc;

    public static void main(String[] args) {
        SparkConf sparkConf = new SparkConf();
        sparkConf.setMaster("local[*]");
        sparkConf.setAppName("Irish flowers Record Reader Transform");
        sc = new JavaSparkContext(sparkConf);

        learnSubsampling();
        learnOneHot(sc);
    }

    private static void learnOneHot(JavaSparkContext sc) {
//        List<String> lines = readFile(DATA_FILE);
//        System.out.println("========================\nInput:");
//        lines.forEach( line -> System.out.println(line));
//        System.out.println("========================\nEnd Input");

        Schema inputSchema = new Schema.Builder()
            .addColumnInteger("id")
            .addColumnCategorical("cate", "Horror", "Comedy", "Action")
            .addColumnInteger("x_cate")
            .addColumnCategorical("class","0","1")
            .build();

        TransformProcess tp = new TransformProcess.Builder(inputSchema)
            .removeColumns("id")
            .integerToOneHot("x_cate",0,100)
            .categoricalToOneHot("cate")
            .build();


        JavaRDD<String> lines = sc.textFile(DATA_FILE);

        System.out.println("Input: ");
        lines.collect().forEach(line -> System.out.println(line));

        JavaRDD<List<Writable>> samples = lines.map(new StringToWritablesFunction(new CSVRecordReader()));


        // run our transform process
        JavaRDD<List<Writable>> processed = SparkTransformExecutor.execute(samples,tp);

        // convert Writable back to string for export
        JavaRDD<String> toSave= processed.map(new WritablesToStringFunction(","));

        List<String> list = toSave.collect();
        System.out.println("Ouput");
        list.forEach(line -> System.out.println(line));

    }

    private static void learnSubsampling() {
        //ok: there is no subsampling, do it yourself :))
    }
}
