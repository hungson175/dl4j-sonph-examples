package org.deeplearning4j.examples.sonph.examples.avazu;


import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.examples.sonph.examples.ModelWriter;
import org.deeplearning4j.examples.sonph.examples.MyScoreIterationListener;
import org.deeplearning4j.examples.sonph.examples.iris.Helpers;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.nd4j.jita.conf.CudaEnvironment;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.buffer.DataBuffer;
import org.nd4j.linalg.api.buffer.util.DataTypeUtil;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.IOException;

import static org.deeplearning4j.examples.feedforward.regression.RegressionMathFunctions.learningRate;
import static org.deeplearning4j.examples.feedforward.regression.RegressionMathFunctions.numOutputs;

/**
 * Notes about performance:
 *
 * CPU
 * - 256 x 10 batch 256
 *  1000 62s
 *  2000 126s
 *  3000 190s
 *  4000 253s
 *  5000 316s
 *  Avg = 63s / 1000 iterations
 *  Avg/row: 316,000 / (5000*256) = 0.24 ms / row
 *
 *  - 16384 x 1 batch 256
 *  1000: 150s
 *  2000: 314s
 *  Avg: 157s
 *  Avg/row:
 *
 *  - 256 x 10 batch 4096
 *  50: 33s
 *  100: 62s
 *  150: 91s
 *  200: 120s
 *  Avg: 60s / 100 iterations
 *  -> Avg/row: 120,000 / ( 200 * 4096) = 0.14 ms /row
 *
 *
 *  GPU
 *
 *  - 256 x 10 batch 256
 *  1000: est 125s
 *  2000
 *  3000
 *  29000: 9011s
 *  Avg: 310s / 1000 iterations
 *  Avg/row: 9,011,000 / (29000 * 256) = 1.21 ms / row
 *
 *  - 16384 x 1 batch 256
 *  1000: 309s
 *  2000: 627s
 *  Avg: 313s / 1000 iterations
 *
 *  - 256 x 10 batch 4096:
 *  50: 54s
 *  100: 106s
 *  Avg: 106s / 100 iterations
 *  Avg/row: 106,000 / (100 * 4096) = 0.258 ms / row
 *
 *  - 16384 x 1 batch 4096
 *  50: 58s
 *  100: 112s
 *  Avg: 112s / 1000 iterations
 *  Avg/row: 112,000 / (100 * 4096) = 0.27 ms / row
 *
 *
 * B16384 x N256 x L10:
 * - GPU
 * 10: 70s, 70s
 * 20: 132s, 134s
 * 30: 194s
 * Avg: 65s / 10 iter ->
 * Avg 1 row: 194,000 / (30 x 16384) = 0.39 ms / row
 *
 * - CPU (without/with sincos(hour))
 * 10: 30s, 30s
 * 20: 56s, 56s
 * 30: 80s, 82s
 * 40: 105s, 108s
 * 50: 130s, 135s
 *
 * Avg/row: 130,000 / (50 * 16384) = 0.15 ms / row
 *
 *
 * B512 x N2048 x L16 (hour encoded sin/cos)
 * CPU:
 * 10: 22s
 * 20: 41s
 * 30: 60s
 * 40: 80s
 * 50: 99s
 *
 * Avg/row = 99,000 / (50*512) = 3.8 ms/row
 *
 * GPU:
 * 100: 76s
 * 200: 149s
 * 300: 223s
 *
 * Avg/row: 223,000 / ( 300 * 512) = 1.35ms / row
 *
 * B512 x N512 x L64
 * CPU
 * 10: 8s
 * 20: 16s
 * 30: 23s
 * 40: 30s
 * 50: 37s
 * 100: 73s
 * Avg/row: 73,000 / (100*512) =   1.42 ms / row
 *
 * GPU
 * 10: 7s
 * 20: 12s
 * 30: 16s
 * 40: 21s
 * 50: 26s
 * 100: 48s
 * Avg/row: 48,000 / (100*512) = 0.9375 ms/row
 *
 *
 * B512 x N4096 x L8:
 * CPU
 * 10: 37s
 * 20: 69s
 * 30: 101s
 * 40: 135s
 * 50: 168s
 * Avg/row: 168,000 / (512 * 50) = 6.56 ms/row
 *
 * GPU:
 * 10: 14s
 * 20: 25s
 * 30: 35s
 * 40: 45s
 * 50: 56s
 * Avg/row: 56,000 / ( 512 * 50 ) = 2.18 ms/row
 *
 *
 *
 */
public class AvazuCTR {
    private static final int LABEL_INDEX = 0; //TODO: later
    private static final long SEED = 1;
    private static final double LEARNING_RATE = 0.1;
    public static final double L2_RATE = 0.01;
    private static final int BATCH_SIZE = 512;
    private static final int N_EPOCHS = 100;
    public static final double MOMENTUM = 0.9;
//    public static final String TRAIN_CSV = "/home/hungson175/Data/Avazu/train_subl_0p125_processed.csv";
//    public static final String TEST_CSV = "/home/hungson175/Data/Avazu/test_subl_0p025_processed.csv";
    public static final String TRAIN_CSV = "/home/hungson175/Data/Avazu/train_merged.csv";
    public static final String TEST_CSV = "/home/hungson175/Data/Avazu/dev_merged.csv";
    private static final int N_INPUTS = 247;
    private static final int N_OUTPUTS = 2;
    private static final int N_LAYERS_COUNT = 16;
    private static final int N_NODES_EACH_LAYER = 512;
    private static final String CONFIG_STRING = "LR:" + LEARNING_RATE + "_BATCH:" + BATCH_SIZE + "_L:" + N_LAYERS_COUNT + "_NODES:" + N_NODES_EACH_LAYER;



    public static void main(String[] args) {
        System.out.println(CONFIG_STRING);

        DataTypeUtil.setDTypeForContext(DataBuffer.Type.HALF);
        CudaEnvironment.getInstance().getConfiguration().allowMultiGPU(true);
        CudaEnvironment.getInstance().getConfiguration()
            .setMaximumDeviceCacheableLength(4L*1024 * 1024 * 1024L)
            .setMaximumDeviceCache(6L * 1024 * 1024 * 1024L)
            .setMaximumHostCacheableLength(4L*1024 * 1024 * 1024L)
            .setMaximumHostCache(6L * 1024 * 1024 * 1024L);


//        UIServer uiServer = UIServer.getInstance();
//        //Configure where the network information (gradients, score vs. time etc) is to be stored. Here: store in memory.
//        //Alternative: new FileStatsStorage(File), for saving and loading later
//        StatsStorage statsStorage = new InMemoryStatsStorage();
//        uiServer.attach(statsStorage);


        int[] N_HIDDEN_LAYERS = new int[N_LAYERS_COUNT];
        for (int i = 0; i < N_LAYERS_COUNT; i++) {
            N_HIDDEN_LAYERS[i] = N_NODES_EACH_LAYER;
        }

        try {

            RecordReader trainReader = new CSVRecordReader();
            trainReader.initialize(new FileSplit(new File(TRAIN_CSV)));
            DataSetIterator trainIter = new RecordReaderDataSetIterator(trainReader, BATCH_SIZE, LABEL_INDEX, 2);

            RecordReader testReader = new CSVRecordReader();
            testReader.initialize(new FileSplit(new File(TEST_CSV)));
            DataSetIterator testIter = new RecordReaderDataSetIterator(testReader, BATCH_SIZE, LABEL_INDEX, 2);

            MultiLayerConfiguration conf = Helpers.createNN(
                N_INPUTS, N_OUTPUTS, N_HIDDEN_LAYERS,
                new Nesterovs(LEARNING_RATE, MOMENTUM),
                0.001,
                LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD,
                Activation.SOFTMAX,
                SEED);

            long startTime = System.currentTimeMillis();

            MultiLayerNetwork model = new MultiLayerNetwork(conf);
            model.init();
            model.setListeners(
                new MyScoreIterationListener(10),
                new ModelWriter(1000, "/media/hungson175/DataStorage/Data/Avazu", "model_N"+N_NODES_EACH_LAYER+"xL"+N_LAYERS_COUNT+"xB"+BATCH_SIZE)
            );    //Print score every 100 parameter updates

            System.out.println("Toal epochs: " + N_EPOCHS);
            for ( int n = 0; n < N_EPOCHS; n++) {
                model.fit( trainIter );
                System.out.println(CONFIG_STRING);
                System.out.println("Runtime until n-th epoch done: " + ((System.currentTimeMillis()  - startTime) / 100.0) + " seconds");
            }

            System.out.println("Evaluate model.... "+CONFIG_STRING);
            Evaluation eval = new Evaluation(N_OUTPUTS);
            while(testIter.hasNext()){
                DataSet t = testIter.next();
                INDArray features = t.getFeatureMatrix();
                INDArray labels = t.getLabels();
                INDArray predicted = model.output(features,false);

                eval.eval(labels, predicted);
            }

            System.out.println(eval.stats());
            System.out.println("CONFIG: " + CONFIG_STRING);
            System.out.println("Total runtime: " + ((System.currentTimeMillis() - startTime)/1000.0) + " s");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
