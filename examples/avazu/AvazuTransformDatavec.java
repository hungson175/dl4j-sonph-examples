package org.deeplearning4j.examples.sonph.examples.avazu;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.transform.TransformProcess;
import org.datavec.api.transform.schema.Schema;
import org.datavec.api.transform.transform.time.DeriveColumnsFromTimeTransform;
import org.datavec.api.writable.Writable;
import org.datavec.spark.transform.SparkTransformExecutor;
import org.datavec.spark.transform.misc.StringToWritablesFunction;
import org.datavec.spark.transform.misc.WritablesToStringFunction;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;

import java.io.*;
import java.util.List;
import java.util.Random;

public class AvazuTransformDatavec {

    static private final double SUBSAMPLING_RATE_TRAIN = 0.125;
    static private final String SUBSAMPLING_RATE_TRAIN_STR = "0p125";
    static private final double SUBSAMPLING_RATE_TEST = 0.025;
    static private final String SUBSAMPLING_RATE_TEST_STR = "0p025";

    protected static String BASE_DIR = "/home/hungson175/Data/Avazu";


    private static final String TRAIN_CSV = BASE_DIR + "/" + "train.csv";
    private static final String DEV_CSV = BASE_DIR + "/" + "train.csv";
    protected static final String TEST_CSV = BASE_DIR + "/"+ "test_noheader.csv";

    protected  static String SUBSAMPLE_TRAIN_FILE_PATH = BASE_DIR + "/" + "train_subl_" + SUBSAMPLING_RATE_TRAIN + ".csv";
    protected static String SUBSAMPLE_DEV_FILE_PATH = BASE_DIR + "/" + "test_subl_" + SUBSAMPLING_RATE_TEST + ".csv";

    protected static String PROCESSED_TRAIN_SPARK_DIR = BASE_DIR + "/" + "train_subl_" + SUBSAMPLING_RATE_TRAIN + "_processed.csv";
    protected static String PROCESSED_TEST_SPARK_DIR = BASE_DIR + "/" + "test_subl_" + SUBSAMPLING_RATE_TEST + "_processed.csv";
    protected static String PROCESSED_TEST_SPARK_DIR_PATH = BASE_DIR + "/" +"test_processed.csv";

    protected static String PROCESSED_TRAIN_FILE_PATH = BASE_DIR + "/" + "train_subl_" + SUBSAMPLING_RATE_TRAIN_STR + "_processed.csv";
    protected static String PROCESSED_DEV_FILE_PATH = BASE_DIR + "/" + "test_subl_" + SUBSAMPLING_RATE_TEST_STR + "_processed.csv";
    protected static String PROCESSED_TEST_FILE_PATH = BASE_DIR + "/" +"test_noheader_processed.csv";

    public static final int REPORT_LINES_COUNT = 10000;
    private static JavaSparkContext sc;


    public static void main(String[] args) throws IOException {
        /**
         * Let's do this
         * 1. Train: 40M lines -> shuffle, get only 5M lines
         * 2. Test: 1M lines -> shuffle, get only 125k lines
         *
         * Which features to use ?
         * id: remove
         * click: sure
         * hour: 240 reduce 24, to transform , only keep hour -> one hot
         * C1: 7 , one hot
         * banner_pos: 7, one hot
         * site_id: removed
         * site_domain: removed
         * app_id: removed
         * app_domain: removed (559)
         * app_category: 36, one hot
         * device_id/ip: removed
         * device_type: 5, one hot
         * C14: removed
         * C15: 8, one hot
         * C16: 9, one hot
         * C17: removed
         * C18: 4, one hot
         * C19: 68, one hot
         * C20: removed
         * C21: 60, one hot
         *
         */

        SparkConf sparkConf = new SparkConf();
        sparkConf.setMaster("local[*]");
        sparkConf.setAppName("Avazu Record Reader Transform");
        sc = new JavaSparkContext(sparkConf);

        transformTrainData();
        transformTestData();

    }

    private static void transformTestData() throws IOException {
        transformData(sc,TEST_CSV, PROCESSED_TEST_SPARK_DIR_PATH, false);
    }

    private static void transformTrainData() throws IOException {
        long seed = 1;

        subSamplingData(TRAIN_CSV, SUBSAMPLE_TRAIN_FILE_PATH, SUBSAMPLING_RATE_TRAIN, seed);
        subSamplingData(DEV_CSV, SUBSAMPLE_DEV_FILE_PATH, SUBSAMPLING_RATE_TEST, seed>>1);

        transform(SUBSAMPLE_TRAIN_FILE_PATH, SUBSAMPLE_DEV_FILE_PATH, PROCESSED_TRAIN_SPARK_DIR, PROCESSED_TEST_SPARK_DIR);

        System.out.println(SUBSAMPLE_TRAIN_FILE_PATH);
    }

    private static void transform(
        String subSampleDataFilePath, String subSampleTestFilePath,
        String processedTrainFilePath, String processedTestFilePath) throws IOException {


        transformData(sc, subSampleDataFilePath, processedTrainFilePath, true);
        transformData(sc, subSampleTestFilePath, processedTestFilePath, true);
    }

    public final static String[] APP_CATE = {
        "07d7df22","f95efa07","0f2161f8","8ded1f7a","09481d60",
        "cef3e649","d1327cf5","75d80bbe","dc97ec06","fc6fa53d",
        "4ce2e9fc","a3c42688","0f9a328c","7113d72a","a7fd01ec",
        "879c24eb","a86a3e89","4681bb9d","79f0b860","2281a340",
        "8df2e842","5326cf99","18b1e0be","2fc4f2aa","71af18ce",
        "0bfbc358","4b7ade46","bf8ac856","0d82db25","6fea3693",
        "86c1a5a3","ef03ae90","f395a87f","bd41f328","cba0e20d","52de74cf"};

    public final static String[] C1_VALUES = {"1005","1010","1002","1012","1007","1001","1008"};
    public final static String[] C15_VALUES = {"300","320","216","728","120","1024","480","768"};
    public final static String[] C16_VALUES = {"250","50","36","90","480","20","768","320","1024"};
    public final static String[] C19_VALUES = {
        "39","35","41","167","675","1063","161","171","297",
        "679","33","303","169","175","935","1327","431","427",
        "419","803","47","34","163","43","1059","687","553",
        "811","1451","681","937","547","809","815","813",
        "290","425","291","1711","299","38","1319","551",
        "423","559","943","1065","1831","295","417","1575",
        "801","1071","1315","289","1835","673","1839","939",
        "555","45","1583","1447","683","545","1195","677","1959","1199"};

    final static String[] C21_VALUES = {
        "33","117","157","159","23","16","221","68","48",
        "71","61","79","82","212","52","32","20","15","112",
        "91","51","229","43","156","13","76","110","42","95",
        "101","100","46","17","70","90","246","93","69","116",
        "108","253","111","194","35","171","204","163","94",
        "182","126","251","178","255","104","1","177","102","195","219","85","240","184"};

    private static void transformData(JavaSparkContext sc, String filePath, String processedFilePath, boolean withLabel) throws IOException {
        Schema.Builder inputSchemaBuilder = new Schema.Builder();
        inputSchemaBuilder.addColumnsString("id");
        if ( withLabel ) inputSchemaBuilder.addColumnInteger("click");
        inputSchemaBuilder
            .addColumnsString("hour")
            .addColumnCategorical("C1", C1_VALUES)
            .addColumnInteger("banner_pos")
            .addColumnsString("site_id","site_domain","site_category","app_id","app_domain")
            .addColumnCategorical("app_category", APP_CATE)
            .addColumnsString("device_id","device_ip","device_model")
            .addColumnsInteger("device_type","device_conn_type","C14")
            .addColumnCategorical("C15", C15_VALUES)
            .addColumnCategorical("C16", C16_VALUES)
            .addColumnsInteger("C17","C18")
            .addColumnCategorical("C19", C19_VALUES)
            .addColumnsInteger("C20")
            .addColumnCategorical("C21", C21_VALUES)
            .build();

        Schema inputSchema = inputSchemaBuilder.build();

        //TODO: verify number of columns in the output file ! compute vs. real output
        TransformProcess tp = new TransformProcess.Builder(inputSchema)
            .removeColumns("id","site_id","site_domain","site_category","app_id","app_domain","device_id","device_ip","device_model","C14","C17","C20")
            //hour: later
            .stringToTimeTransform("hour", "yyMMddHH", DateTimeZone.UTC)
            .transform(new DeriveColumnsFromTimeTransform.Builder("hour")
                    .addIntegerDerivedColumn("hourOfDay", DateTimeFieldType.hourOfDay())
                    .build())
            .removeColumns("hour")
            .integerToOneHot("hourOfDay",0,23)
//            .convertToDouble("hourOfDay")
//            .renameColumn("hourOfDay", "hourOfDayDouble")
//            .doubleMathOp("hourOfDayDouble",MathOp.Divide, 24)
//            .doubleMathOp("hourOfDayDouble",MathOp.Multiply, 2 * Math.PI)
//            .duplicateColumn("hourOfDayDouble","sinHour")
//            .doubleMathFunction("sinHour",MathFunction.SIN)
//            .duplicateColumn("hourOfDayDouble","cosHour")
//            .doubleMathFunction("cosHour",MathFunction.COS)
//            .removeColumns("hour","hourOfDayDouble")
            .categoricalToOneHot("C1")
            .integerToOneHot("banner_pos",0,7)
            .categoricalToOneHot("app_category")
            .integerToOneHot("device_type",0,5)
            .integerToOneHot("device_conn_type",0,5)
            .categoricalToOneHot("C15")
            .categoricalToOneHot("C16")
            .integerToOneHot("C18",0,3)
            .categoricalToOneHot("C19")
            .categoricalToOneHot("C21")
            .build();

        int numCols = 1 + C1_VALUES.length + 8 + APP_CATE.length + 6 + 6 + C15_VALUES.length + C16_VALUES.length + 4 + C19_VALUES.length + C21_VALUES.length;
        System.out.println("Number of ouput columns: " + numCols);

        JavaRDD<String> lines = sc.textFile(filePath);
        // convert to Writable
        JavaRDD<List<Writable>> samples = lines.map(new StringToWritablesFunction(new CSVRecordReader()));
        // run our transform process
        JavaRDD<List<Writable>> processed = SparkTransformExecutor.execute(samples,tp);
        // convert Writable back to string for export
        JavaRDD<String> toSave= processed.map(new WritablesToStringFunction(","));

//        try (PrintWriter printWriter = new PrintWriter(new FileWriter(processedFilePath))) {
//            List<String> list = toSave.collect();
//            list.forEach(line -> printWriter.println(line));
//        }
        toSave.saveAsTextFile(processedFilePath);

    }

    private static void subSamplingData(String dataFilePath, String subSamplFilePath, double subsamplingRate, long seed) throws IOException {
        Random random = new Random(seed);
        int cntLine = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(dataFilePath));
             PrintWriter printWriter = new PrintWriter(new FileWriter(subSamplFilePath));
        ) {
            String line = null;
            line = br.readLine(); //skip header line
            while ( (  line = br.readLine()) != null) {
                cntLine++;
                double v = random.nextDouble();
                if ( v <= subsamplingRate ) {
                    printWriter.println(line);
                }
                if ( cntLine % REPORT_LINES_COUNT == 0) {
                    System.out.println("Line: " + cntLine);
                }
            }
        }
    }
}
