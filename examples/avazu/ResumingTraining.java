package org.deeplearning4j.examples.sonph.examples.avazu;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.examples.sonph.examples.Helpers;
import org.deeplearning4j.examples.sonph.examples.ModelWriter;
import org.deeplearning4j.examples.sonph.examples.MyScoreIterationListener;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.io.File;
import java.io.IOException;

public class ResumingTraining {
    public static final String BASE_DIR = "/home/hungson175/Data/Avazu/";
    public static final String TRAIN_CSV = BASE_DIR + "train_subl_0p125_processed.csv";
    public static final String TEST_CSV = BASE_DIR + "test_subl_0p025_processed.csv";

    private static final int LABEL_INDEX = 0;

    private static final int BATCH_SIZE = 512;

    public static final String MODEL_FILE_NAME = "model_N1024xL4xB512_95000.model";
    public static final String MODEL_PATH = BASE_DIR + MODEL_FILE_NAME;
    private static final int N_EPOCHS = 100;
    public static final String BASE_MODEL_DIR = "/media/hungson175/DataStorage/Data/Avazu";

    public static void main(String[] args) throws Exception {
        new ResumingTraining().start();
    }

    private void start() throws IOException, InterruptedException {
        MultiLayerNetwork model = Helpers.readModel(MODEL_PATH);

        RecordReader trainReader = new CSVRecordReader();
        trainReader.initialize(new FileSplit(new File(TRAIN_CSV)));
        DataSetIterator trainIter = new RecordReaderDataSetIterator(trainReader, BATCH_SIZE, LABEL_INDEX, 2);

        RecordReader testReader = new CSVRecordReader();
        testReader.initialize(new FileSplit(new File(TEST_CSV)));
        DataSetIterator testIter = new RecordReaderDataSetIterator(testReader, BATCH_SIZE, LABEL_INDEX, 2);

        System.out.println("Total epochs: " + N_EPOCHS);
        model.setListeners(
            new MyScoreIterationListener(10),
            new ModelWriter(1000, BASE_MODEL_DIR, "model_resume_" + MODEL_FILE_NAME)
        );    //Print score every 100 parameter updates

        long startTime = System.currentTimeMillis();
        for ( int n = 0; n < N_EPOCHS; n++) {
            model.fit( trainIter );
            System.out.println(MODEL_PATH);
            System.out.println("Runtime until n-th epoch done: " + ((System.currentTimeMillis()  - startTime) / 100.0) + " seconds");
        }
    }
}
