package org.deeplearning4j.examples.sonph.examples.avazu;

import org.jcodec.common.Assert;

import java.io.*;
import java.util.HashMap;

public class AvazuDataMerger {

    public static final int IMP_THRESHOLD = 100;
    private int cntAnomali = 0;

    public static void main(String[] args) throws IOException {
        /**
         * Read the map from the Counting file to get the category counting maps
         * Read the data file generated from the DataVec transformation (* pls transform the time to OneHot)
         * For each line of the subsampling training file
         *  - write the corresponding line from datavec file
         *  - For each considering field:
         *      read the key k of that field - get value imp = mapImp[field][k], clicks = mapClick[field][k]
         *      if imp > 100 -> ctr = clicks/imp else ctr = 0
         *      append ctr of that field to the end of line
         * For each line of the subsampling testing file:
         *  - do the same as training file
         *
         */
        new AvazuDataMerger().start();
    }

    private HashMap<String, HashMap<String, Integer>> countClick = new HashMap<>();
    private HashMap<String, HashMap<String, Integer>> countImp = new HashMap<>();
    private HashMap<String, Integer> indexOfField = new HashMap<>();

    private void start() throws IOException {
        readMaps();
        System.out.println("Done reading map");


        //generate train file
//        generateMergedFile(
//            AvazuTransformDatavec.PROCESSED_TRAIN_FILE_PATH,
//            AvazuTransformDatavec.SUBSAMPLE_TRAIN_FILE_PATH,
//            AvazuTransformDatavec.BASE_DIR + "train_merged.csv",
//            true);
//        generateMergedFile(
//            AvazuTransformDatavec.PROCESSED_DEV_FILE_PATH,
//            AvazuTransformDatavec.SUBSAMPLE_DEV_FILE_PATH,
//            AvazuTransformDatavec.BASE_DIR + "dev_merged.csv",
//            true);

        generateMergedFile(
            AvazuTransformDatavec.PROCESSED_TEST_FILE_PATH,
            AvazuTransformDatavec.TEST_CSV,
            AvazuTransformDatavec.BASE_DIR + "/" +"test_merged.csv",
            false
        );


    }

    private void generateMergedFile(
        String processedFilePath,
        String subSampleFilePath,
        String mergeFilePath, boolean gotClickField) throws IOException {
        String lineDataVec = null;
        String lineSubsample = null;
        int cntLine = 0;
        cntAnomali = 0;
        try (
            BufferedReader transformedDataReader = new BufferedReader(new FileReader(processedFilePath));
            BufferedReader subsampleDataReader = new BufferedReader(new FileReader(subSampleFilePath));
            PrintWriter writer = new PrintWriter(new FileWriter(mergeFilePath));)
        {

            prepare(gotClickField);
            long startTime = System.currentTimeMillis();
            while ( ( lineDataVec = transformedDataReader.readLine()) != null ) {
                lineSubsample = subsampleDataReader.readLine();
                String generatedFeaturesLine = generateFeatures(cntLine, lineSubsample);
                StringBuffer sb = new StringBuffer(lineDataVec.trim());
                sb.append(",").append(generatedFeaturesLine);
                writer.println(sb.toString());
                if ( cntLine % 10000 == 0 ) {
                    System.out.printf("Processed line: %d , runtime: %s s\n", cntLine, (System.currentTimeMillis() - startTime) / 1000);
                }
                cntLine++;
            }

        }
        System.out.println("Problematic lines: "  + cntAnomali);
    }

    private void prepare(boolean gotClickField) {
        for (String field : EncodeCategoryCTR.FIELDS_TO_COUNT) {
            for (int i = 0; i < EncodeCategoryCTR.ALL_FIELDS.length; i++) {
                if ( EncodeCategoryCTR.ALL_FIELDS[i].equals(field)) {
                    int value = i;
                    if ( ! gotClickField ) value--;
                    indexOfField.put(field, value);
                    break;
                }
            }

        }
        Assert.assertEquals(EncodeCategoryCTR.FIELDS_TO_COUNT.length , indexOfField.keySet().size());

    }

    private String generateFeatures(
        int lineCount,
        String lineSubsample)
    {
        StringBuffer sb = new StringBuffer();
        //TODO: wow !! care the test file doesn't contain the click columns !
        String[] words = lineSubsample.split(",");
        for (String field : EncodeCategoryCTR.FIELDS_TO_COUNT) {
            String fieldKey = words[indexOfField.get(field)];
            Integer clicks = countClick.get(field).get(fieldKey);
            Integer imps = countImp.get(field).get(fieldKey);
            if ( imps == null || clicks == null ) {
//                System.out.println("Debug:");
//                System.out.printf("\t#%d line : %s\n",lineCount,lineSubsample);
//                System.out.println("\tfield: " + field + " - key: " + fieldKey);
//                System.out.println("\tclicks: " + clicks + " -- imps: " + imps);
//                System.out.println("\t keyset imp: " + countImp.get(field).keySet());
//                System.out.println("Null imp/clicks at line: " + lineCount);
                cntAnomali++;
            }
            if ( clicks == null) clicks = 0;
            if ( imps == null) imps = 1;

            double ctr = 0.0;
            if ( imps > IMP_THRESHOLD ) ctr = 1.0 * clicks / imps;
            sb.append(ctr).append(",");
        }

       //if not the last feature, append comma ","

        String rsWithComma = sb.toString();
        return rsWithComma.substring(0, rsWithComma.length() - 1);
    }

    private void readMaps() throws IOException {
        for (String field : EncodeCategoryCTR.FIELDS_TO_COUNT) {
            countClick.put(field, new HashMap<>());
            countImp.put(field, new HashMap<>());
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(EncodeCategoryCTR.COUNT_RESULTS_FILE))) {
            String line = null;
            while ( (line = reader.readLine()) != null ) {

                String[] words = line.split(" ");
                Assert.assertEquals(2, words.length);
                String field = words[0].trim();
                int numKeys = Integer.parseInt(words[1].trim());
                for (int i = 0; i < numKeys; i++) {
                    line = reader.readLine();
                    words = line.split(" ");
                    Assert.assertEquals(3, words.length);
                    String key = words[0].trim();
                    int clicks = Integer.parseInt(words[1].trim());
                    int imps = Integer.parseInt(words[2].trim());
//                    if (field.equals("site_domain") && key.equals("bb1ef334")) {
//                        System.out.println("specific line: " + line);
//                    }
                    addToMap(countClick, field, key, clicks);
                    addToMap(countImp, field, key, imps);

                }
            }
        }

        //assertFieldCounting();
        Assert.assertEquals(EncodeCategoryCTR.FIELDS_TO_COUNT.length, countClick.keySet().size());
        Assert.assertEquals(EncodeCategoryCTR.FIELDS_TO_COUNT.length, countImp.keySet().size());
    }


    private void addToMap(
        HashMap< String, HashMap<String,Integer> > fieldMap,
        String field,
        String key,
        int value) {

        fieldMap.get(field).put(key, value);
    }
}
