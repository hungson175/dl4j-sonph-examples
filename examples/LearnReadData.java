package org.deeplearning4j.examples.sonph.examples;


import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.examples.sonph.examples.iris.Helpers;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.IOException;

public class LearnReadData {

    public static final int NUM_OUTPUTS = 3;
    public static final int NUM_INPUTS = 4;
    public static final int NUM_EPOCHS = 10;

    //    final static String TEST_FILE = "/home/hungson175/Data/Iris/iris_test_without_lael.txt";
    public static void main(String[] args) throws IOException, InterruptedException {

        String baseDir = "/home/hungson175/Data/Iris/";

        String trainFile = baseDir + "iris_train_processed.csv";
        String testFile = baseDir + "iris_test_without_label.csv";


        RecordReader trainRR = new CSVRecordReader();
        trainRR.initialize(new FileSplit(new File(trainFile)));
        DataSetIterator trainIter = new RecordReaderDataSetIterator(trainRR, 16, 4, NUM_OUTPUTS);

        DataNormalization normalizer = new NormalizerStandardize();
        normalizer.fit(trainIter);
        trainIter.setPreProcessor(normalizer);

        MultiLayerConfiguration conf = Helpers.createNN(
            NUM_INPUTS, NUM_OUTPUTS, new int[]{100, 100, 100},
            new Nesterovs(0.01, 0.98),
            0.001,
            LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD,
            Activation.SOFTMAX,
            1);

        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();
        model.setListeners(new ScoreIterationListener(5));
        System.out.println("Training model ...");
        for (int i = 0; i < NUM_EPOCHS; i++) {
            model.fit(trainIter);
        }


        RecordReader testRR = new CSVRecordReader();
        testRR.initialize(new FileSplit(new File(testFile)));
        DataSetIterator testIter = new RecordReaderDataSetIterator(testRR, 16);

        Evaluation eval = new Evaluation(3);
        while ( testIter.hasNext() ) {
            DataSet ds = testIter.next();
            INDArray featureMatrix = ds.getFeatureMatrix();
            INDArray output = model.output(featureMatrix);
//            System.out.println(output);
        }
    }
}
