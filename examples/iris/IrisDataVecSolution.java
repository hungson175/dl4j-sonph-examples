package org.deeplearning4j.examples.sonph.examples.iris;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.transform.TransformProcess;
import org.datavec.api.transform.schema.Schema;
import org.datavec.api.writable.Writable;
import org.datavec.spark.transform.SparkTransformExecutor;
import org.datavec.spark.transform.misc.StringToWritablesFunction;
import org.datavec.spark.transform.misc.WritablesToStringFunction;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.ops.impl.indexaccum.IMax;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class IrisDataVecSolution {
    static final int[] N_HIDDEN_LAYERS = {100};//,200,200,200,200,200,200,200,200};
    public static final int NUM_EPOCHS = 10;

    static final long SEED = 1;
    static final int N_IN = 4;
    static final int N_OUTPUT = 3;
    static final double LEARNING_RATE = 0.001;


    public static void main(String[] args) throws IOException, InterruptedException {
        String baseDir = "/home/hungson175/Data/Iris/";
        System.out.println("Basedir: " + baseDir);
        String dataFile = baseDir + "iris.data";
        String trainFile = baseDir + "iris_train.data";
        String testFile = baseDir + "iris_test.data";

        String processedTrainFile = baseDir + "iris_train_processed.csv";
        String processedTestFile = baseDir + "iris_test_processed.csv";

        System.out.println(trainFile + " _ " + testFile);



        Helpers.splitData(
            dataFile,
            trainFile,
            testFile,
            SEED,
            0.8);


        SparkConf sparkConf = new SparkConf();
        sparkConf.setMaster("local[*]");
        sparkConf.setAppName("Irish flowers Record Reader Transform");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        processFile(sc, trainFile, processedTrainFile);
        processFile(sc, testFile, processedTestFile);


        MultiLayerConfiguration conf = Helpers.createNN(
            N_IN,N_OUTPUT, N_HIDDEN_LAYERS,
            new Nesterovs(LEARNING_RATE, 0.98),
            LEARNING_RATE * 0.005,
            LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD,
            Activation.SOFTMAX,
            SEED);

        runDeepNetwork(conf, processedTrainFile, processedTestFile, NUM_EPOCHS);
    }




    private static void processFile(JavaSparkContext sc, String dataFile, String processedFile) throws IOException {
        Schema inputSchema = new Schema.Builder()
            .addColumnsDouble("slen","swidth","plen","pwidth")
            .addColumnCategorical("clazz","Iris-setosa", "Iris-versicolor", "Iris-virginica")
            .build();

        TransformProcess tp = new TransformProcess.Builder(inputSchema)
            .categoricalToInteger("clazz")
            .build();


        System.out.println("==========================\nInput: ");
        JavaRDD<String> lines = sc.textFile(dataFile);
//        List<String> list = lines.collect();
//        for (String s : list) {
//            System.out.println("["+s+"]");
//        }
//        System.out.println("============================\n\n");


        JavaRDD<List<Writable>> flowerSamples = lines.map(new StringToWritablesFunction(new CSVRecordReader()));
        // run our transform process
        JavaRDD<List<Writable>> processed = SparkTransformExecutor.execute(flowerSamples,tp);
        // convert Writable back to string for export
        JavaRDD<String> toSave= processed.map(new WritablesToStringFunction(","));

        List<String> outLines = toSave.collect();


        try (PrintWriter writer = new PrintWriter(new FileWriter(processedFile))) {
            for (String outLine : outLines) {
                writer.println(outLine);
            }
        }
//        toSave.saveAsTextFile(processedFile);
    }

    private static void runDeepNetwork(
        MultiLayerConfiguration conf,
        String trainFile,
        String testFile,
        int numEpochs) throws IOException, InterruptedException {

        DataSetIterator trainIter = readCSVDataset(trainFile, 16, 4, 3);


        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();
        //print the score with every 1 iteration
        String baseDir = "/home/hungson175/Data/Iris";
        String baseFileName = "saved_model";
        model.setListeners(
            new ScoreIterationListener(1),
            new ModelWriter(20,baseDir, baseFileName));

        System.out.println("Train model....");
        for( int i=0; i<numEpochs; i++ ){
            model.fit(trainIter);
        }

        //save model
        String modelFileName = "/home/hungson175/Data/Iris/model_save.zip";
        File locationToSave = Helpers.saveModel(model, modelFileName);


        MultiLayerNetwork loadedModel = ModelSerializer.restoreMultiLayerNetwork(locationToSave);




//        System.out.println("\n\n=======================\nModel from disk: ");
//        evalModel(testFile, loadedModel);

        System.out.println("\n\n=======================\nOriginal model: ");
        evalModel(testFile, model);


//        evalIter(testFile, baseDir, baseFileName, 20);
//        evalIter(testFile, baseDir, baseFileName, 40);
//        evalIter(testFile, baseDir, baseFileName, 60);
//        evalIter(testFile, baseDir, baseFileName, 100);
//        evalIter(testFile, baseDir, baseFileName, 200);
//        evalIter(testFile, baseDir, baseFileName, 400);



    }

    private static void evalIter(String testFile, String baseDir, String baseFileName, int evalIter) throws IOException, InterruptedException {
        System.out.println("\n\n=======================\nSaved model at " + evalIter + ":");
        evalModel(testFile, ModelSerializer.restoreMultiLayerNetwork(new File(baseDir+"/"+baseFileName+"_"+ evalIter +".model")));
    }

    private static void evalModel(String testFile, MultiLayerNetwork model) throws IOException, InterruptedException {
        DataSetIterator testIter = readCSVDataset(testFile, 16, 4, 3);
        System.out.println("Evaluate model....");
        Evaluation eval = new Evaluation(3); //create an evaluation object with 10 possible classes
        while(testIter.hasNext()){
            DataSet next = testIter.next();
            INDArray output = model.output(next.getFeatureMatrix()); //get the networks prediction
            INDArray maxIndices = Nd4j.getExecutioner().exec(new IMax(output), 1);
            System.out.println("\n======\nIMax Exec:"+ maxIndices + "\n\n" + output);
            for(int i = 0 ; i < output.size(0 ); i++) {
                int maxIndex = (int) maxIndices.getDouble(i);
                System.out.println("Max Index: " + maxIndex + " , value: " + output.getDouble(i, maxIndex ));
            }
            eval.eval(next.getLabels(), output); //check the prediction against the true class
        }

        System.out.println(eval.stats());
        System.out.println("****************Example finished********************");
    }

    private static DataSetIterator readCSVDataset(
        String csvFileClasspath, int batchSize, int labelIndex, int numClasses)
        throws IOException, InterruptedException{

        RecordReader rr = new CSVRecordReader();
        rr.initialize(new FileSplit(new File(csvFileClasspath)) ) ;
        DataSetIterator iterator = new RecordReaderDataSetIterator(rr,batchSize,labelIndex,numClasses);
        return iterator;
    }
}
