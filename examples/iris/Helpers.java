package org.deeplearning4j.examples.sonph.examples.iris;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.util.ModelSerializer;
import org.jetbrains.annotations.NotNull;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.IUpdater;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.*;
import java.util.Random;

public class Helpers {
    public static void splitData(
        String originalFile,
        String trainFile,
        String testFile,
        long seed,
        double trainSplitRatio) throws IOException {
        Random random = new Random(seed);
        try (BufferedReader br = new BufferedReader(new FileReader(originalFile)) ;
             PrintWriter trainWriter = new PrintWriter(new FileWriter(trainFile)) ;
             PrintWriter testWriter = new PrintWriter(new FileWriter(testFile)) ;
        ) {
            String line = null;
            while ( ( line = br.readLine()) != null) {
                double v = random.nextDouble();
                if ( v < trainSplitRatio ) {
                    trainWriter.println(line);
                } else {
                    testWriter.println(line);
                }

            }
        }

    }

    public static MultiLayerConfiguration createNN(
        int nIn,
        int nOut,
        int[] nHiddenLayers,
        IUpdater updater,
        double regL2,
        LossFunctions.LossFunction lossFn,
        Activation outputActFn,
        long seed) {

        NeuralNetConfiguration.ListBuilder list = new NeuralNetConfiguration.Builder()
            .seed(seed) //include a random seed for reproducibility
            .weightInit(WeightInit.XAVIER)
            .updater(updater)
            .l2(regL2) // regularize learning model
            .list();
        int[] nAtLayers = new int[nHiddenLayers.length + 1];

        nAtLayers[0] = nIn;
        for (int i = 1; i < nAtLayers.length; i++) {
            nAtLayers[i] = nHiddenLayers[i - 1];
        }
        for (int i = 0; i < nAtLayers.length - 1; i++) {
            list.layer( i, new DenseLayer.Builder() //create the first input layer.
                .nIn(nAtLayers[i]).nOut(nAtLayers[i+1])
                .activation(Activation.RELU)
                .build());
        }
        list.layer(nAtLayers.length - 1, new OutputLayer.Builder(lossFn) //create hidden layer
            .activation(outputActFn)
            .nIn(nAtLayers[nAtLayers.length-1])
            .nOut(nOut)
            .build());

        return list.build();

    }

    @NotNull
    public static File saveModel(Model model, String modelFileName) throws IOException {
        File locationToSave = new File(modelFileName);
        boolean saveUpdater = true;
        ModelSerializer.writeModel(model, locationToSave, saveUpdater);
        return locationToSave;
    }
}
