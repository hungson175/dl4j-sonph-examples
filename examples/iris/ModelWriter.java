package org.deeplearning4j.examples.sonph.examples.iris;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.optimize.api.BaseTrainingListener;

import java.io.IOException;

class ModelWriter extends BaseTrainingListener {

    private final String baseDir;
    private final String baseFileName;
    int period;
    public ModelWriter(int periodToSave, String baseDir, String baseFileName) {
        this.period = periodToSave;
        this.baseDir = baseDir;
        this.baseFileName = baseFileName;
    }

    @Override
    public void iterationDone(Model model, int iteration, int epoch) {
        //do nothing
        String baseFilePath = baseDir +"/"+ baseFileName ;
        if ( iteration % period == 0 ) {
            String fileName = baseFilePath +"_" + iteration + ".model";
            try {
                Helpers.saveModel(model, fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
