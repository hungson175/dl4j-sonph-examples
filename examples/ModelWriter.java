package org.deeplearning4j.examples.sonph.examples;

import org.deeplearning4j.examples.sonph.examples.iris.Helpers;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.optimize.api.BaseTrainingListener;

import java.io.IOException;

public class ModelWriter extends BaseTrainingListener {

    private final String baseDir;
    private final String baseFileName;
    private final Object lockObject = new Object();

    int period;
    private int lastSavedIteration;

    public ModelWriter(int periodToSave, String baseDir, String baseFileName) {
        this.period = periodToSave;
        this.baseDir = baseDir;
        this.baseFileName = baseFileName;
    }

    @Override
    public void iterationDone(Model model, int iteration, int epoch) {
        //do nothing

        String baseFilePath = baseDir +"/"+ baseFileName ;
        if ( iteration > 0 && iteration % period == 0 ) {
            synchronized (lockObject) {
                if ( iteration == lastSavedIteration ) return;
                System.out.println("Modelwriter saved at:" + epoch +"#" + iteration);
                lastSavedIteration = iteration;
                String fileName = baseFilePath + "_" + iteration + ".model";
                try {
                    Helpers.saveModel(model, fileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
